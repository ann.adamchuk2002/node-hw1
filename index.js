const express = require('express');
const morgan = require('morgan');
const router = require('./router')

const app = express();

const PORT = 8080

app.use(express.json());
app.use(morgan('tiny'));

app.use('/', router)
app.use('/', express.static('files'))
app.use((req,res) => {
    res.status(400).json({ "message": "Client error" })
})

app.listen(PORT, () => {
    console.log(`Server started on port ${PORT}`)
});
