const fsPromise = require('fs').promises;
const path = require('path');
const fs = require('fs');

const createFile = async (req, res) => {
    const {filename, content} = req.body;
    const dir = path.parse(req.url).base;
    if(!fs.existsSync(dir)) {
        fs.mkdirSync(dir)
    }
    const fList = await fsPromise.readdir('./files');

    if (!filename || !content) {
        const param = filename ? 'content' : 'filename';
        res.status(400).json({message: `No '${param}' parameter`});
        return;
    }

    const fExt = filename.slice(filename.lastIndexOf('.'));

    if(!(['.log', '.txt', '.json', '.yaml', '.xml', '.js'].includes(fExt))) {
        res.status(400).json({message: "Not supported file extention"});
    }
    else if (fList.includes(filename)) {
        res.status(400).json({message: `Cannot create file! ${filename} already exists`});
    }
    else {
        try {
            console.log(filename);
            await fsPromise.writeFile(`./files/${filename}`, content, 'utf-8');
            res.status(200).json({message: "File created successfully"});
        } catch (error) {
            res.status(500).json({message: "Server error"})
        }
    }
}

const getFiles = async (req, res) => {
    try {
        const fList = await fsPromise.readdir(`./files`);
        res.status(200).json({
                message: "Success",
                files: fList
            })
    } catch (error) {
        res.status(500)
            .json({"message": "Server error"})
    }
}

const getFile = async (req, res) => {
    const filename = req.params.filename;
    const fExt = path.parse(req.url).ext;
    const fList = await fsPromise.readdir(`./files`);
    if (!fList.includes(filename)) {
        res.status(400).json({
            message: `No file with '${filename}' filename found`
        });
    }
    else {
        try {
            const content = await fsPromise.readFile(`./files/${filename}`, 'utf-8');
            const createDate = (await fsPromise.stat(`./files/${filename}`)).birthtime;
            res.status(200).json({
                message: "Success",
                filename: filename,
                content: content,
                extension: fExt.slice(1),
                uploadedDate: createDate
            });
        } catch (error) {
            res.status(500).json({
                message: "Server error"
            });
        }
    }
}

const deleteFile = async (req, res) => {
    const filename = req.params.filename;
    const filesList = await fsPromise.readdir(`./files`);
    
    if (!filesList.includes(filename)) {
        res.status(400).json({
            message: `No file with '${filename}' filename found`
        });
    }
    else {
        try {
            await fsPromise.unlink(`./files/${filename}`);
            res.status(200).json({message: `File ${filename} has been deleted successfully`});
        } catch (error) {
            res.status(500).json({message: "Server error"});
        }
    }
}

const putFile = async (req, res) => {
    const filename = req.params.filename;
    const {content} = req.body;
    const filesList = await fsPromise.readdir(`./files`);
    
    if (!filesList.includes(filename)) {
        res.status(400).json({
            message: `No file with '${filename}' filename found`
        });
    }
    else {
        try {
            await fsPromise.writeFile(`./files/${filename}`, content, 'utf-8');
            res.status(200).json({
                message: `File ${filename} has been changed successfully`
            });
        } catch (e) {
            res.status(500)
                .json({"message": "Server error"});
        }
    }
}

module.exports = {
    createFile,
    getFiles,
    getFile,
    putFile,
    deleteFile
};
